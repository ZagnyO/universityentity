﻿namespace UniversityWF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.comboBDepartment = new System.Windows.Forms.ComboBox();
            this.comboBSpeciality = new System.Windows.Forms.ComboBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.labelSpeciality = new System.Windows.Forms.Label();
            this.labelDepartment = new System.Windows.Forms.Label();
            this.buttonStudent = new System.Windows.Forms.Button();
            this.buttonSubject = new System.Windows.Forms.Button();
            this.buttonTeacher = new System.Windows.Forms.Button();
            this.buttonSchedule = new System.Windows.Forms.Button();
            this.buttonScheduleChange = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(147, 260);
            this.listBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(350, 64);
            this.listBox1.TabIndex = 1;
            // 
            // comboBDepartment
            // 
            this.comboBDepartment.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBDepartment.FormattingEnabled = true;
            this.comboBDepartment.Location = new System.Drawing.Point(703, 181);
            this.comboBDepartment.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBDepartment.Name = "comboBDepartment";
            this.comboBDepartment.Size = new System.Drawing.Size(350, 28);
            this.comboBDepartment.TabIndex = 2;
            this.comboBDepartment.Text = "Кафедры";
            this.comboBDepartment.SelectedIndexChanged += new System.EventHandler(this.comboBDepartment_SelectedIndexChanged);
            // 
            // comboBSpeciality
            // 
            this.comboBSpeciality.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBSpeciality.FormattingEnabled = true;
            this.comboBSpeciality.Location = new System.Drawing.Point(147, 181);
            this.comboBSpeciality.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBSpeciality.Name = "comboBSpeciality";
            this.comboBSpeciality.Size = new System.Drawing.Size(350, 28);
            this.comboBSpeciality.TabIndex = 3;
            this.comboBSpeciality.Text = "Специальности";
            this.comboBSpeciality.SelectedIndexChanged += new System.EventHandler(this.comboBSpeciality_SelectedIndexChanged);
            // 
            // listBox2
            // 
            this.listBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 20;
            this.listBox2.Location = new System.Drawing.Point(703, 260);
            this.listBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(350, 64);
            this.listBox2.TabIndex = 4;
            // 
            // labelSpeciality
            // 
            this.labelSpeciality.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelSpeciality.AutoSize = true;
            this.labelSpeciality.Location = new System.Drawing.Point(157, 146);
            this.labelSpeciality.Name = "labelSpeciality";
            this.labelSpeciality.Size = new System.Drawing.Size(263, 20);
            this.labelSpeciality.TabIndex = 5;
            this.labelSpeciality.Text = "Специальности университета\r\n";
            // 
            // labelDepartment
            // 
            this.labelDepartment.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelDepartment.AutoSize = true;
            this.labelDepartment.Location = new System.Drawing.Point(708, 146);
            this.labelDepartment.Name = "labelDepartment";
            this.labelDepartment.Size = new System.Drawing.Size(224, 20);
            this.labelDepartment.TabIndex = 6;
            this.labelDepartment.Text = "Информация о кафедрах";
            // 
            // buttonStudent
            // 
            this.buttonStudent.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonStudent.Location = new System.Drawing.Point(147, 525);
            this.buttonStudent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonStudent.Name = "buttonStudent";
            this.buttonStudent.Size = new System.Drawing.Size(350, 50);
            this.buttonStudent.TabIndex = 8;
            this.buttonStudent.Text = "Информация о студентах";
            this.buttonStudent.UseVisualStyleBackColor = true;
            this.buttonStudent.Click += new System.EventHandler(this.buttonStudent_Click);
            // 
            // buttonSubject
            // 
            this.buttonSubject.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonSubject.Location = new System.Drawing.Point(147, 362);
            this.buttonSubject.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonSubject.Name = "buttonSubject";
            this.buttonSubject.Size = new System.Drawing.Size(350, 50);
            this.buttonSubject.TabIndex = 10;
            this.buttonSubject.Text = "Информация об изучаемых предметах";
            this.buttonSubject.UseVisualStyleBackColor = true;
            this.buttonSubject.Click += new System.EventHandler(this.buttonSubject_Click);
            // 
            // buttonTeacher
            // 
            this.buttonTeacher.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonTeacher.Location = new System.Drawing.Point(147, 444);
            this.buttonTeacher.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonTeacher.Name = "buttonTeacher";
            this.buttonTeacher.Size = new System.Drawing.Size(350, 50);
            this.buttonTeacher.TabIndex = 11;
            this.buttonTeacher.Text = "Информация о преподавателях";
            this.buttonTeacher.UseVisualStyleBackColor = true;
            this.buttonTeacher.Click += new System.EventHandler(this.buttonTeacher_Click);
            // 
            // buttonSchedule
            // 
            this.buttonSchedule.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonSchedule.Location = new System.Drawing.Point(703, 362);
            this.buttonSchedule.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonSchedule.Name = "buttonSchedule";
            this.buttonSchedule.Size = new System.Drawing.Size(350, 50);
            this.buttonSchedule.TabIndex = 12;
            this.buttonSchedule.Text = "Расписание";
            this.buttonSchedule.UseVisualStyleBackColor = true;
            this.buttonSchedule.Click += new System.EventHandler(this.buttonSchedule_Click);
            // 
            // buttonScheduleChange
            // 
            this.buttonScheduleChange.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonScheduleChange.Location = new System.Drawing.Point(703, 495);
            this.buttonScheduleChange.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonScheduleChange.Name = "buttonScheduleChange";
            this.buttonScheduleChange.Size = new System.Drawing.Size(350, 50);
            this.buttonScheduleChange.TabIndex = 13;
            this.buttonScheduleChange.Text = "Внести изменения\r\n в расписание";
            this.buttonScheduleChange.UseVisualStyleBackColor = true;
            this.buttonScheduleChange.Click += new System.EventHandler(this.buttonScheduleChange_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(157, 235);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "Обучаются группы";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(708, 235);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(326, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "Преподаватели выбранной кафедры";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(367, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(440, 24);
            this.label3.TabIndex = 16;
            this.label3.Text = "Университет информационных технологий";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1210, 620);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonScheduleChange);
            this.Controls.Add(this.buttonSchedule);
            this.Controls.Add(this.buttonTeacher);
            this.Controls.Add(this.buttonSubject);
            this.Controls.Add(this.buttonStudent);
            this.Controls.Add(this.labelDepartment);
            this.Controls.Add(this.labelSpeciality);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.comboBSpeciality);
            this.Controls.Add(this.comboBDepartment);
            this.Controls.Add(this.listBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Университет";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ComboBox comboBDepartment;
        private System.Windows.Forms.ComboBox comboBSpeciality;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label labelSpeciality;
        private System.Windows.Forms.Label labelDepartment;
        private System.Windows.Forms.Button buttonStudent;
        private System.Windows.Forms.Button buttonSubject;
        private System.Windows.Forms.Button buttonTeacher;
        private System.Windows.Forms.Button buttonSchedule;
        private System.Windows.Forms.Button buttonScheduleChange;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

