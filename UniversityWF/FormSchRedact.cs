﻿using Demo.Domain;
using Domain.lib.Entities;
using System;
using Demo.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormSchRedact : Form
    {
        public FormSchRedact()
        {
            InitializeComponent();
        }

        private void FormSchRedact_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = Unit.GroupsRepository.AllItems.ToList();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Group item = (sender as ComboBox).SelectedItem as Group;
            List<Lecture> l = new List<Lecture>();
          
            foreach (Lecture uu in Unit.LecturesRepository.AllItems
                .Where(x => x.Schedule.DayWeek == "Понедельник")
                .Where(x => x.Group.Id == item.Id)
                .ToList())
            {
                l.Add(uu);
            }
            dataGridView1.DataSource = l.ToList();

            l = new List<Lecture>();
            foreach (Lecture uu in Unit.LecturesRepository.AllItems
                .Where(x => x.Schedule.DayWeek == "Вторник")
                .Where(x => x.Group.Id == item.Id)
                .ToList())
            {
                l.Add(uu);
            }
            dataGridView2.DataSource = l.ToList();

            l = new List<Lecture>();
            foreach (Lecture uu in Unit.LecturesRepository.AllItems
                .Where(x => x.Schedule.DayWeek == "Среда")
                .Where(x => x.Group.Id == item.Id)
                .ToList())
            {
                l.Add(uu);
            }
            dataGridView3.DataSource = l.ToList();
            l = new List<Lecture>();
            foreach (Lecture uu in Unit.LecturesRepository.AllItems
                .Where(x => x.Schedule.DayWeek == "Четверг")
                .Where(x => x.Group.Id == item.Id)
                .ToList())
            {
                l.Add(uu);
            }
            dataGridView4.DataSource = l.ToList();
            l = new List<Lecture>();
            foreach (Lecture uu in Unit.LecturesRepository.AllItems
                .Where(x => x.Schedule.DayWeek == "Пятница")
                .Where(x => x.Group.Id == item.Id)
                .ToList())
            {
                l.Add(uu);
            }
            dataGridView5.DataSource = l.ToList();
            l = new List<Lecture>();
            foreach (Lecture uu in Unit.LecturesRepository.AllItems
                .Where(x => x.Schedule.DayWeek == "Суббота")
                .Where(x => x.Group.Id == item.Id)
                .ToList())
            {
                l.Add(uu);
            }
            dataGridView6.DataSource = l.ToList();
        }
    }
}
