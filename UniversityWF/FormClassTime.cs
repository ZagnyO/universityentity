﻿using Demo.Domain;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormClassTime : Form
    {
        public FormClassTime()
        {
            InitializeComponent();
        }

        private void FormClassTime_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.ClassTimeRepository.AllItems.ToList();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            FormCTRedact formCTRedact = new FormCTRedact();
            DialogResult result = formCTRedact.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            ClassTime ob = new ClassTime();
            ob.Number = Convert.ToInt32(formCTRedact.comboBox1.SelectedItem);
            //ob.StartLecture = formCTRedact.dateTimePickerStart.Value;
            //ob.FinishLecture = formCTRedact.dateTimePickerFinish.Value;
            ob.StartLecture = formCTRedact.textBox1.Text;
            ob.FinishLecture = formCTRedact.textBox2.Text;


            Unit.ClassTimeRepository.AddItem(ob);
            Unit.ClassTimeRepository.SaveChanges();
            dataGridView1.DataSource = Unit.ClassTimeRepository.AllItems.ToList();
            MessageBox.Show("Новый объект добавлен");
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Unit.ClassTimeRepository.DeleteItem(id);
                Unit.ClassTimeRepository.SaveChanges();
                dataGridView1.DataSource = Unit.ClassTimeRepository.AllItems.ToList();
                MessageBox.Show("Объект удален");
            }
        }
    }
}
