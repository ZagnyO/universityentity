﻿using Demo.Domain;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormStudent : Form
    {
        public FormStudent()
        {
            InitializeComponent();

        }
        private void FormStudent_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.StudentsRepository.AllItems.ToList();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            FormSTRedact formSTRedact = new FormSTRedact();

            foreach (Group g in Unit.GroupsRepository.AllItems)
            {
                formSTRedact.comboBox1.Items.Add(g);
            }
            //        formSTRedact.comboBox1.DisplayMember = "Name";
            //        formSTRedact.comboBox1.ValueMember = "Id";

            DialogResult result = formSTRedact.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            Student student = new Student();
            student.Birthday = formSTRedact.dateTimePicker1.Value.Date;
            student.FirstName = formSTRedact.textBox2.Text;
            student.MiddleName = formSTRedact.textBox3.Text;
            student.LastName = formSTRedact.textBox1.Text;
            student.LogbookNumber = Int32.Parse(formSTRedact.textBox4.Text);
            student.Email = formSTRedact.textBox5.Text;
            student.Address = formSTRedact.textBox6.Text;
            student.Group = (Group)formSTRedact.comboBox1.SelectedItem;

            Unit.StudentsRepository.AddItem(student);
            Unit.StudentsRepository.SaveChanges();
            MessageBox.Show("Новый объект добавлен");
            dataGridView1.DataSource = Unit.StudentsRepository.AllItems.ToList();
        }
        //    редактирование
        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Student student = new Student();
                student = Unit.StudentsRepository.GetItem(id);
                FormSTRedact formSTRedact = new FormSTRedact();
                foreach (Group g in Unit.GroupsRepository.AllItems)
                {
                    formSTRedact.comboBox1.Items.Add(g);
                }
                formSTRedact.textBox1.Text = student.LastName;
                formSTRedact.textBox2.Text = student.FirstName;
                formSTRedact.textBox3.Text = student.MiddleName;
                formSTRedact.dateTimePicker1.Value = student.Birthday;
                formSTRedact.textBox4.Text = Convert.ToString(student.LogbookNumber);
                formSTRedact.textBox5.Text = student.Email;
                formSTRedact.textBox6.Text = student.Address;
                formSTRedact.comboBox1.SelectedItem = student.Group;

                DialogResult result = formSTRedact.ShowDialog(this);
                if (result == DialogResult.Cancel)
                    return;

                student.Birthday = formSTRedact.dateTimePicker1.Value;
                student.FirstName = formSTRedact.textBox2.Text;
                student.MiddleName = formSTRedact.textBox3.Text;
                student.LastName = formSTRedact.textBox1.Text;
                student.LogbookNumber = Int32.Parse(formSTRedact.textBox4.Text);
                student.Email = formSTRedact.textBox5.Text;
                student.Address = formSTRedact.textBox6.Text;
                student.Group = (Group)formSTRedact.comboBox1.SelectedItem;

                Unit.StudentsRepository.SaveChanges();
                dataGridView1.Refresh(); // обновляем грид
                MessageBox.Show("Объект обновлен");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Unit.StudentsRepository.DeleteItem(id);
                Unit.StudentsRepository.SaveChanges();
                MessageBox.Show("Объект удален");
                dataGridView1.DataSource = Unit.StudentsRepository.AllItems.ToList();
            }
        }

        private void buttonPhone_Click(object sender, EventArgs e)
        {
            FormPhone formPhone = new FormPhone();
            DialogResult result = formPhone.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;
        }

        private void buttonMark_Click(object sender, EventArgs e)
        {
            FormMark formMark = new FormMark();
            DialogResult result = formMark.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;
        }

        private void buttonPhoneShow_Click(object sender, EventArgs e)
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel3.Controls.Add(this.listBox1);
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(12, 4);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(446, 84);
            this.listBox1.TabIndex = 2;
            int index = 0;
            int id = 0;
            if (dataGridView1.SelectedRows.Count > 0)
            {
                index = dataGridView1.SelectedRows[0].Index;
                id = Int32.Parse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString());
            }
            else
                return;
            listBox1.DataSource = Unit.PhonesRepository.AllItems.Where(x => x.Student.Id == id).ToList();
        }
        public System.Windows.Forms.ListBox listBox1;

        private void buttonMarkShow_Click(object sender, EventArgs e)
        {
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.panel3.Controls.Add(this.listBox2);
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 16;
            this.listBox2.Location = new System.Drawing.Point(12, 4);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(546, 84);
            this.listBox2.TabIndex = 2;
            int index = 0;
            int id = 0;
            if (dataGridView1.SelectedRows.Count > 0)
            {
                index = dataGridView1.SelectedRows[0].Index;
                id = Int32.Parse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString());
            }
            else
                return;
            listBox2.DataSource = Unit.MarksRepository.AllItems.Where(x => x.Student.Id == id).ToList();

        }
        public System.Windows.Forms.ListBox listBox2;

        private void buttonPhoneShowCancel_Click(object sender, EventArgs e)
        {
            listBox1.Visible = false;
        }

        private void buttonMarkShowCancel_Click(object sender, EventArgs e)
        {
            listBox2.Visible = false;
        }
    }
}