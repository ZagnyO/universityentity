﻿using Demo.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormPhone : Form
    {
        public FormPhone()
        {
            InitializeComponent();
        }

        private void FormPhone_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.PhonesRepository.AllItems.ToList();
        }
    }
}
