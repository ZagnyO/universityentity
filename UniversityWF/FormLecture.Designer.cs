﻿namespace UniversityWF
{
    partial class FormLecture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonLecAdd = new System.Windows.Forms.Button();
            this.buttonChange = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonClassRoom = new System.Windows.Forms.Button();
            this.buttonClassTime = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lectureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.classTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classroomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teachSubjDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.classTimeDataGridViewTextBoxColumn,
            this.classroomDataGridViewTextBoxColumn,
            this.teachSubjDataGridViewTextBoxColumn,
            this.groupDataGridViewTextBoxColumn,
            this.scheduleDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.lectureBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(835, 246);
            this.dataGridView1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(741, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Закрыть";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // buttonLecAdd
            // 
            this.buttonLecAdd.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.buttonLecAdd.Location = new System.Drawing.Point(212, 0);
            this.buttonLecAdd.Name = "buttonLecAdd";
            this.buttonLecAdd.Size = new System.Drawing.Size(93, 23);
            this.buttonLecAdd.TabIndex = 3;
            this.buttonLecAdd.Text = "Добавить";
            this.buttonLecAdd.UseVisualStyleBackColor = false;
            this.buttonLecAdd.Click += new System.EventHandler(this.buttonLecAdd_Click);
            // 
            // buttonChange
            // 
            this.buttonChange.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.buttonChange.Location = new System.Drawing.Point(350, 0);
            this.buttonChange.Name = "buttonChange";
            this.buttonChange.Size = new System.Drawing.Size(94, 23);
            this.buttonChange.TabIndex = 4;
            this.buttonChange.Text = "Изменить";
            this.buttonChange.UseVisualStyleBackColor = false;
            this.buttonChange.Click += new System.EventHandler(this.buttonChange_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.buttonDelete.Location = new System.Drawing.Point(492, 0);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(85, 23);
            this.buttonDelete.TabIndex = 5;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Location = new System.Drawing.Point(145, -77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonClassRoom);
            this.panel2.Controls.Add(this.buttonClassTime);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 321);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(835, 37);
            this.panel2.TabIndex = 10;
            // 
            // buttonClassRoom
            // 
            this.buttonClassRoom.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.buttonClassRoom.Location = new System.Drawing.Point(264, 6);
            this.buttonClassRoom.Name = "buttonClassRoom";
            this.buttonClassRoom.Size = new System.Drawing.Size(117, 23);
            this.buttonClassRoom.TabIndex = 9;
            this.buttonClassRoom.Text = "Аудитории";
            this.buttonClassRoom.UseVisualStyleBackColor = false;
            this.buttonClassRoom.Click += new System.EventHandler(this.buttonClassRoom_Click);
            // 
            // buttonClassTime
            // 
            this.buttonClassTime.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.buttonClassTime.Location = new System.Drawing.Point(32, 6);
            this.buttonClassTime.Name = "buttonClassTime";
            this.buttonClassTime.Size = new System.Drawing.Size(192, 23);
            this.buttonClassTime.TabIndex = 8;
            this.buttonClassTime.Text = "Расписание звонков";
            this.buttonClassTime.UseVisualStyleBackColor = false;
            this.buttonClassTime.Click += new System.EventHandler(this.buttonClassTime_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(835, 42);
            this.panel3.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(309, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Информация о лекциях";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.buttonLecAdd);
            this.panel4.Controls.Add(this.buttonChange);
            this.panel4.Controls.Add(this.buttonDelete);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 288);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(835, 33);
            this.panel4.TabIndex = 12;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dataGridView1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 42);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(835, 246);
            this.panel5.TabIndex = 13;
            // 
            // lectureBindingSource
            // 
            this.lectureBindingSource.DataSource = typeof(Domain.lib.Entities.Lecture);
            // 
            // classTimeDataGridViewTextBoxColumn
            // 
            this.classTimeDataGridViewTextBoxColumn.DataPropertyName = "ClassTime";
            this.classTimeDataGridViewTextBoxColumn.FillWeight = 61.54822F;
            this.classTimeDataGridViewTextBoxColumn.HeaderText = "Лента";
            this.classTimeDataGridViewTextBoxColumn.Name = "classTimeDataGridViewTextBoxColumn";
            // 
            // classroomDataGridViewTextBoxColumn
            // 
            this.classroomDataGridViewTextBoxColumn.DataPropertyName = "Classroom";
            this.classroomDataGridViewTextBoxColumn.FillWeight = 61.54822F;
            this.classroomDataGridViewTextBoxColumn.HeaderText = "Аудитория";
            this.classroomDataGridViewTextBoxColumn.Name = "classroomDataGridViewTextBoxColumn";
            // 
            // teachSubjDataGridViewTextBoxColumn
            // 
            this.teachSubjDataGridViewTextBoxColumn.DataPropertyName = "TeachSubj";
            this.teachSubjDataGridViewTextBoxColumn.FillWeight = 253.8071F;
            this.teachSubjDataGridViewTextBoxColumn.HeaderText = "Преподаватель - Предмет";
            this.teachSubjDataGridViewTextBoxColumn.Name = "teachSubjDataGridViewTextBoxColumn";
            // 
            // groupDataGridViewTextBoxColumn
            // 
            this.groupDataGridViewTextBoxColumn.DataPropertyName = "Group";
            this.groupDataGridViewTextBoxColumn.FillWeight = 61.54822F;
            this.groupDataGridViewTextBoxColumn.HeaderText = "Группа";
            this.groupDataGridViewTextBoxColumn.Name = "groupDataGridViewTextBoxColumn";
            // 
            // scheduleDataGridViewTextBoxColumn
            // 
            this.scheduleDataGridViewTextBoxColumn.DataPropertyName = "Schedule";
            this.scheduleDataGridViewTextBoxColumn.FillWeight = 61.54822F;
            this.scheduleDataGridViewTextBoxColumn.HeaderText = "День недели";
            this.scheduleDataGridViewTextBoxColumn.Name = "scheduleDataGridViewTextBoxColumn";
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // FormLecture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(835, 358);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormLecture";
            this.Text = "FormLecture";
            this.Load += new System.EventHandler(this.FormLecture_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonLecAdd;
        private System.Windows.Forms.Button buttonChange;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonClassRoom;
        private System.Windows.Forms.Button buttonClassTime;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridViewTextBoxColumn classTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn classroomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teachSubjDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scheduleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource lectureBindingSource;
    }
}