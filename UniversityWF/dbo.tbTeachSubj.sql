﻿CREATE TABLE [dbo].[tbTeachSubj] (
    [Id]         INT IDENTITY (1, 1) NOT NULL,
    [TeachId]    INT NOT NULL,
    [SubjId]     INT NOT NULL,
    [Subject_Id] INT NULL,
    [Teacher_Id] INT NULL,
    CONSTRAINT [PK_dbo.tbTeachSubj] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.tbTeachSubj_dbo.tbSubjects_Subject_Id] FOREIGN KEY ([Subject_Id]) REFERENCES [dbo].[tbSubjects] ([Id]),
    CONSTRAINT [FK_dbo.tbTeachSubj_dbo.TbTeachers_Teacher_Id] FOREIGN KEY ([Teacher_Id]) REFERENCES [dbo].[TbTeachers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Subject_Id]
    ON [dbo].[tbTeachSubj]([Subject_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Teacher_Id]
    ON [dbo].[tbTeachSubj]([Teacher_Id] ASC);

