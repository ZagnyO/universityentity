﻿using Demo.Domain;
using Demo.Domain.Entities;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBSpeciality.DataSource = Unit.SpecialitiesRepository.AllItems.ToList();
            comboBDepartment.DataSource = Unit.DepartmentsRepository.AllItems.ToList();
        }

        private void comboBSpeciality_SelectedIndexChanged(object sender, EventArgs e)
        {
            Speciality item = (sender as ComboBox).SelectedItem as Speciality;
            listBox1.DataSource =
                Unit.GroupsRepository.AllItems.Where(x => x.Speciality.Id == item.Id).ToList();
        }
        private void comboBDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            Department item = (sender as ComboBox).SelectedItem as Department;
            listBox2.DataSource =
                Unit.TeachersRepository.AllItems.Where(x => x.Department.Id == item.Id).ToList();
        }

        private void buttonStudent_Click(object sender, EventArgs e)
        {
            FormStudent formStudent = new FormStudent();
            DialogResult result = formStudent.ShowDialog(this);

            if (result == DialogResult.Cancel)
                return;
        }

        private void buttonSubject_Click(object sender, EventArgs e)
        {
            FormSubject formSubject = new FormSubject();
            DialogResult result = formSubject.ShowDialog(this);

            if (result == DialogResult.Cancel)
                return;
        }

        private void buttonTeacher_Click(object sender, EventArgs e)
        {
            FormTeacher formTeacher = new FormTeacher();
            DialogResult result = formTeacher.ShowDialog(this);

            if (result == DialogResult.Cancel)
                return;
        }

        private void buttonSchedule_Click(object sender, EventArgs e)
        {
            //FormSchedule formSchedule = new FormSchedule();
            //DialogResult result = formSchedule.ShowDialog(this);

            //if (result == DialogResult.Cancel)
            //    return;
            FormSchRedact formSchRedact = new FormSchRedact();
            DialogResult result = formSchRedact.ShowDialog(this);

            if (result == DialogResult.Cancel)
                return;

        }

        private void buttonScheduleChange_Click(object sender, EventArgs e)
        {
            FormLecture formLecture = new FormLecture();
            DialogResult result = formLecture.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;
        }
    }
}
