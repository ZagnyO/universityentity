﻿using Demo.Domain;
using Demo.Domain.Entities;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormTeachSubj : Form
    {
        public FormTeachSubj()
        {
            InitializeComponent();
        }
        private void FormTeachSubj_Load(object sender, EventArgs e)
        {
                dataGridView1.DataSource = Unit.TeachSubjRepository.AllItems.ToList();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            FormTeachSubjRedact formTeachSubjRedact = new FormTeachSubjRedact();

            formTeachSubjRedact.comboBox1.DataSource = Unit.TeachersRepository.AllItems.ToList();
           // formTeachSubjRedact.comboBox1.DisplayMember = "LastName";
           // formTeachSubjRedact.comboBox1.ValueMember = "Id";

            formTeachSubjRedact.comboBox2.DataSource = Unit.SubjectsRepository.AllItems.ToList();
            //formTeachSubjRedact.comboBox2.DisplayMember = "Name";
            //formTeachSubjRedact.comboBox2.ValueMember = "Id";
                    
            DialogResult result = formTeachSubjRedact.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            TeachSubj ob = new TeachSubj();
            ob.Teacher = (Teacher)formTeachSubjRedact.comboBox1.SelectedItem;
            ob.Subject = (Subject)formTeachSubjRedact.comboBox2.SelectedItem;
           
            Unit.TeachSubjRepository.AddItem(ob);
            Unit.TeachSubjRepository.SaveChanges();
            dataGridView1.DataSource = Unit.TeachSubjRepository.AllItems.ToList();
            MessageBox.Show("Новый объект добавлен");  
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Unit.TeachSubjRepository.DeleteItem(id);
                Unit.TeachSubjRepository.SaveChanges();
                dataGridView1.DataSource = Unit.TeachSubjRepository.AllItems.ToList();
                MessageBox.Show("Объект удален");
            }
        }       
    }
}
