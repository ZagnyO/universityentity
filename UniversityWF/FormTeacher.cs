﻿using Demo.Domain;
using Demo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormTeacher : Form
    {
        public FormTeacher()
        {
            InitializeComponent();
        }

        private void FormTeacher_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.TeachersRepository.AllItems.ToList();
        }

        private void buttonTeachSubj_Click(object sender, EventArgs e)
        {
            FormTeachSubj formTeachSubj = new FormTeachSubj();
            DialogResult result = formTeachSubj.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormTeachRedact formTeachRedact = new FormTeachRedact();

            foreach (Department g in Unit.DepartmentsRepository.AllItems)
            {
                formTeachRedact.comboBox1.Items.Add(g);
            }
            DialogResult result = formTeachRedact.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            Teacher ob = new Teacher();
            ob.FirstName = formTeachRedact.textBox2.Text;
            ob.MiddleName = formTeachRedact.textBox3.Text;
            ob.LastName = formTeachRedact.textBox1.Text;
            ob.Department = (Department)formTeachRedact.comboBox1.SelectedItem;

            Unit.TeachersRepository.AddItem(ob);
            Unit.TeachersRepository.SaveChanges();
            MessageBox.Show("Новый объект добавлен");
            dataGridView1.DataSource = Unit.TeachersRepository.AllItems.ToList();
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Teacher ob = new Teacher();
                ob = Unit.TeachersRepository.GetItem(id);
                FormTeachRedact formTeachRedact = new FormTeachRedact();

                foreach (Department g in Unit.DepartmentsRepository.AllItems)
                {
                    formTeachRedact.comboBox1.Items.Add(g);
                }

                formTeachRedact.textBox2.Text = ob.FirstName;
                formTeachRedact.textBox3.Text = ob.MiddleName;
                formTeachRedact.textBox1.Text = ob.LastName;
                formTeachRedact.comboBox1.SelectedItem = ob.Department;

                DialogResult result = formTeachRedact.ShowDialog(this);
                if (result == DialogResult.Cancel)
                    return;

                ob.FirstName = formTeachRedact.textBox2.Text;
                ob.MiddleName = formTeachRedact.textBox3.Text;
                ob.LastName = formTeachRedact.textBox1.Text;
                ob.Department = (Department)formTeachRedact.comboBox1.SelectedItem;

                Unit.TeachersRepository.SaveChanges();
                dataGridView1.Refresh();
                MessageBox.Show("Объект обновлен");
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Unit.TeachersRepository.DeleteItem(id);
                Unit.TeachersRepository.SaveChanges();
                dataGridView1.DataSource = Unit.TeachersRepository.AllItems.ToList();
                MessageBox.Show("Объект удален");
               
            }
        }

    }

}
