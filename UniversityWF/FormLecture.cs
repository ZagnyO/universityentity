﻿using Demo.Domain;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormLecture : Form
    {
        public FormLecture()
        {
            InitializeComponent();
        }

        private void FormLecture_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.LecturesRepository.AllItems.ToList();
        }

        private void buttonLecAdd_Click(object sender, EventArgs e)
        {
            FormLecRedact formLecRedact = new FormLecRedact();
            formLecRedact.comboBox1.DataSource = Unit.ClassTimeRepository.AllItems.ToList();
            formLecRedact.comboBox2.DataSource = Unit.ClassroomsRepository.AllItems.ToList();
            formLecRedact.comboBox3.DataSource = Unit.TeachSubjRepository.AllItems.ToList();
            formLecRedact.comboBox4.DataSource = Unit.GroupsRepository.AllItems.ToList();
            formLecRedact.comboBox5.DataSource = Unit.SchedulesRepository.AllItems.ToList();
            DialogResult result = formLecRedact.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            Lecture ob = new Lecture();
            ob.ClassTime = (ClassTime)formLecRedact.comboBox1.SelectedItem;
            ob.Classroom = (Classroom)formLecRedact.comboBox2.SelectedItem;
            ob.TeachSubj = (TeachSubj)formLecRedact.comboBox3.SelectedItem;
            ob.Group = (Group)formLecRedact.comboBox4.SelectedItem;
            ob.Schedule = (Schedule)formLecRedact.comboBox5.SelectedItem;
            Unit.LecturesRepository.AddItem(ob);
            Unit.LecturesRepository.SaveChanges();
            dataGridView1.DataSource = Unit.LecturesRepository.AllItems.ToList();
            MessageBox.Show("Новый объект добавлен");
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Lecture ob = new Lecture();
                ob = Unit.LecturesRepository.GetItem(id);
                FormLecRedact formLecRedact = new FormLecRedact();
                formLecRedact.comboBox1.DataSource = Unit.ClassTimeRepository.AllItems.ToList();
                formLecRedact.comboBox2.DataSource = Unit.ClassroomsRepository.AllItems.ToList();
                formLecRedact.comboBox3.DataSource = Unit.TeachSubjRepository.AllItems.ToList();
                formLecRedact.comboBox4.DataSource = Unit.GroupsRepository.AllItems.ToList();
                formLecRedact.comboBox5.DataSource = Unit.SchedulesRepository.AllItems.ToList();

                formLecRedact.comboBox1.SelectedItem = ob.ClassTime;
                formLecRedact.comboBox2.SelectedItem = ob.Classroom;
                formLecRedact.comboBox3.SelectedItem = ob.TeachSubj;
                formLecRedact.comboBox4.SelectedItem = ob.Group;
                formLecRedact.comboBox5.DataSource = Unit.SchedulesRepository.AllItems.ToList();
                DialogResult result = formLecRedact.ShowDialog(this);
                if (result == DialogResult.Cancel)
                    return;
               
                ob.ClassTime = (ClassTime)formLecRedact.comboBox1.SelectedItem;
                ob.Classroom = (Classroom)formLecRedact.comboBox2.SelectedItem;
                ob.TeachSubj = (TeachSubj)formLecRedact.comboBox3.SelectedItem;
                ob.Group = (Group)formLecRedact.comboBox4.SelectedItem;
                ob.Schedule = (Schedule)formLecRedact.comboBox5.SelectedItem;
                Unit.LecturesRepository.SaveChanges();
                dataGridView1.DataSource = Unit.LecturesRepository.AllItems.ToList();
                dataGridView1.Refresh();
                MessageBox.Show("Объект обновлен");
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Unit.LecturesRepository.DeleteItem(id);
                Unit.LecturesRepository.SaveChanges();
                dataGridView1.DataSource = Unit.LecturesRepository.AllItems.ToList();
                MessageBox.Show("Объект удален");
            }
        }
        private void buttonClassTime_Click(object sender, EventArgs e)
        {
            FormClassTime formClassTime = new FormClassTime();
            DialogResult result = formClassTime.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;
        }

        private void buttonClassRoom_Click(object sender, EventArgs e)
        {
            FormClassroom formClassroom = new FormClassroom();
            DialogResult result = formClassroom.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;
        }
    }
}
