﻿namespace UniversityWF
{
    partial class FormSchRedact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lectureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lectureBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.lectureBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.lectureBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.lectureBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.lectureBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.classTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classroomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teachSubjDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classTimeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classroomDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teachSubjDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classTimeDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classroomDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teachSubjDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classTimeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classroomDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teachSubjDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classTimeDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classroomDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teachSubjDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classTimeDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classroomDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teachSubjDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource5)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Понедельник",
            "Вторник",
            "Среда",
            "Четверг",
            "Пятница",
            "Суббота",
            "Воскресенье"});
            this.comboBox1.Location = new System.Drawing.Point(457, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(174, 24);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(593, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(671, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Закрыть";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.classTimeDataGridViewTextBoxColumn,
            this.classroomDataGridViewTextBoxColumn,
            this.teachSubjDataGridViewTextBoxColumn,
            this.groupDataGridViewTextBoxColumn,
            this.scheduleDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.lectureBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(756, 81);
            this.dataGridView1.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 691);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(756, 30);
            this.panel1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 607);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(756, 84);
            this.panel2.TabIndex = 6;
            // 
            // dataGridView6
            // 
            this.dataGridView6.AutoGenerateColumns = false;
            this.dataGridView6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView6.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.classTimeDataGridViewTextBoxColumn5,
            this.classroomDataGridViewTextBoxColumn5,
            this.teachSubjDataGridViewTextBoxColumn5,
            this.groupDataGridViewTextBoxColumn5,
            this.scheduleDataGridViewTextBoxColumn5,
            this.idDataGridViewTextBoxColumn5});
            this.dataGridView6.DataSource = this.lectureBindingSource5;
            this.dataGridView6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView6.Location = new System.Drawing.Point(0, 0);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(756, 84);
            this.dataGridView6.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 576);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(756, 31);
            this.panel3.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Суббота";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dataGridView5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 501);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(756, 75);
            this.panel4.TabIndex = 8;
            // 
            // dataGridView5
            // 
            this.dataGridView5.AutoGenerateColumns = false;
            this.dataGridView5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView5.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.classTimeDataGridViewTextBoxColumn4,
            this.classroomDataGridViewTextBoxColumn4,
            this.teachSubjDataGridViewTextBoxColumn4,
            this.groupDataGridViewTextBoxColumn4,
            this.scheduleDataGridViewTextBoxColumn4,
            this.idDataGridViewTextBoxColumn4});
            this.dataGridView5.DataSource = this.lectureBindingSource4;
            this.dataGridView5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView5.Location = new System.Drawing.Point(0, 0);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(756, 75);
            this.dataGridView5.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 469);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(756, 32);
            this.panel5.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Пятница";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dataGridView4);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 396);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(756, 73);
            this.panel6.TabIndex = 10;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoGenerateColumns = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.classTimeDataGridViewTextBoxColumn3,
            this.classroomDataGridViewTextBoxColumn3,
            this.teachSubjDataGridViewTextBoxColumn3,
            this.groupDataGridViewTextBoxColumn3,
            this.scheduleDataGridViewTextBoxColumn3,
            this.idDataGridViewTextBoxColumn3});
            this.dataGridView4.DataSource = this.lectureBindingSource3;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.Location = new System.Drawing.Point(0, 0);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(756, 73);
            this.dataGridView4.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label5);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 363);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(756, 33);
            this.panel7.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Четверг";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.dataGridView3);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 282);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(756, 81);
            this.panel8.TabIndex = 12;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.classTimeDataGridViewTextBoxColumn2,
            this.classroomDataGridViewTextBoxColumn2,
            this.teachSubjDataGridViewTextBoxColumn2,
            this.groupDataGridViewTextBoxColumn2,
            this.scheduleDataGridViewTextBoxColumn2,
            this.idDataGridViewTextBoxColumn2});
            this.dataGridView3.DataSource = this.lectureBindingSource2;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(0, 0);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(756, 81);
            this.dataGridView3.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label4);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 248);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(756, 34);
            this.panel9.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Среда\r\n";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.dataGridView2);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 167);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(756, 81);
            this.panel10.TabIndex = 14;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.classTimeDataGridViewTextBoxColumn1,
            this.classroomDataGridViewTextBoxColumn1,
            this.teachSubjDataGridViewTextBoxColumn1,
            this.groupDataGridViewTextBoxColumn1,
            this.scheduleDataGridViewTextBoxColumn1,
            this.idDataGridViewTextBoxColumn1});
            this.dataGridView2.DataSource = this.lectureBindingSource1;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(756, 81);
            this.dataGridView2.TabIndex = 0;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label3);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel11.Location = new System.Drawing.Point(0, 134);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(756, 33);
            this.panel11.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Вторник";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.dataGridView1);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel12.Location = new System.Drawing.Point(0, 53);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(756, 81);
            this.panel12.TabIndex = 16;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label2);
            this.panel13.Controls.Add(this.label1);
            this.panel13.Controls.Add(this.comboBox1);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(756, 53);
            this.panel13.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Понедельник";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(303, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Выберите группу";
            // 
            // lectureBindingSource
            // 
            this.lectureBindingSource.DataSource = typeof(Domain.lib.Entities.Lecture);
            // 
            // lectureBindingSource1
            // 
            this.lectureBindingSource1.DataSource = typeof(Domain.lib.Entities.Lecture);
            // 
            // lectureBindingSource2
            // 
            this.lectureBindingSource2.DataSource = typeof(Domain.lib.Entities.Lecture);
            // 
            // lectureBindingSource3
            // 
            this.lectureBindingSource3.DataSource = typeof(Domain.lib.Entities.Lecture);
            // 
            // lectureBindingSource4
            // 
            this.lectureBindingSource4.DataSource = typeof(Domain.lib.Entities.Lecture);
            // 
            // lectureBindingSource5
            // 
            this.lectureBindingSource5.DataSource = typeof(Domain.lib.Entities.Lecture);
            // 
            // classTimeDataGridViewTextBoxColumn
            // 
            this.classTimeDataGridViewTextBoxColumn.DataPropertyName = "ClassTime";
            this.classTimeDataGridViewTextBoxColumn.FillWeight = 106.599F;
            this.classTimeDataGridViewTextBoxColumn.HeaderText = "Лента";
            this.classTimeDataGridViewTextBoxColumn.Name = "classTimeDataGridViewTextBoxColumn";
            // 
            // classroomDataGridViewTextBoxColumn
            // 
            this.classroomDataGridViewTextBoxColumn.DataPropertyName = "Classroom";
            this.classroomDataGridViewTextBoxColumn.FillWeight = 44.74895F;
            this.classroomDataGridViewTextBoxColumn.HeaderText = "Аудитория";
            this.classroomDataGridViewTextBoxColumn.Name = "classroomDataGridViewTextBoxColumn";
            // 
            // teachSubjDataGridViewTextBoxColumn
            // 
            this.teachSubjDataGridViewTextBoxColumn.DataPropertyName = "TeachSubj";
            this.teachSubjDataGridViewTextBoxColumn.FillWeight = 148.6521F;
            this.teachSubjDataGridViewTextBoxColumn.HeaderText = "Преподаватель - предмет";
            this.teachSubjDataGridViewTextBoxColumn.Name = "teachSubjDataGridViewTextBoxColumn";
            // 
            // groupDataGridViewTextBoxColumn
            // 
            this.groupDataGridViewTextBoxColumn.DataPropertyName = "Group";
            this.groupDataGridViewTextBoxColumn.HeaderText = "Group";
            this.groupDataGridViewTextBoxColumn.Name = "groupDataGridViewTextBoxColumn";
            this.groupDataGridViewTextBoxColumn.Visible = false;
            // 
            // scheduleDataGridViewTextBoxColumn
            // 
            this.scheduleDataGridViewTextBoxColumn.DataPropertyName = "Schedule";
            this.scheduleDataGridViewTextBoxColumn.HeaderText = "Schedule";
            this.scheduleDataGridViewTextBoxColumn.Name = "scheduleDataGridViewTextBoxColumn";
            this.scheduleDataGridViewTextBoxColumn.Visible = false;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // classTimeDataGridViewTextBoxColumn1
            // 
            this.classTimeDataGridViewTextBoxColumn1.DataPropertyName = "ClassTime";
            this.classTimeDataGridViewTextBoxColumn1.FillWeight = 106.599F;
            this.classTimeDataGridViewTextBoxColumn1.HeaderText = "Лента";
            this.classTimeDataGridViewTextBoxColumn1.Name = "classTimeDataGridViewTextBoxColumn1";
            // 
            // classroomDataGridViewTextBoxColumn1
            // 
            this.classroomDataGridViewTextBoxColumn1.DataPropertyName = "Classroom";
            this.classroomDataGridViewTextBoxColumn1.FillWeight = 44.74895F;
            this.classroomDataGridViewTextBoxColumn1.HeaderText = "Аудитория";
            this.classroomDataGridViewTextBoxColumn1.Name = "classroomDataGridViewTextBoxColumn1";
            // 
            // teachSubjDataGridViewTextBoxColumn1
            // 
            this.teachSubjDataGridViewTextBoxColumn1.DataPropertyName = "TeachSubj";
            this.teachSubjDataGridViewTextBoxColumn1.FillWeight = 148.6521F;
            this.teachSubjDataGridViewTextBoxColumn1.HeaderText = "Преподаватель - Предмет";
            this.teachSubjDataGridViewTextBoxColumn1.Name = "teachSubjDataGridViewTextBoxColumn1";
            // 
            // groupDataGridViewTextBoxColumn1
            // 
            this.groupDataGridViewTextBoxColumn1.DataPropertyName = "Group";
            this.groupDataGridViewTextBoxColumn1.HeaderText = "Group";
            this.groupDataGridViewTextBoxColumn1.Name = "groupDataGridViewTextBoxColumn1";
            this.groupDataGridViewTextBoxColumn1.Visible = false;
            // 
            // scheduleDataGridViewTextBoxColumn1
            // 
            this.scheduleDataGridViewTextBoxColumn1.DataPropertyName = "Schedule";
            this.scheduleDataGridViewTextBoxColumn1.HeaderText = "Schedule";
            this.scheduleDataGridViewTextBoxColumn1.Name = "scheduleDataGridViewTextBoxColumn1";
            this.scheduleDataGridViewTextBoxColumn1.Visible = false;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Visible = false;
            // 
            // classTimeDataGridViewTextBoxColumn3
            // 
            this.classTimeDataGridViewTextBoxColumn3.DataPropertyName = "ClassTime";
            this.classTimeDataGridViewTextBoxColumn3.FillWeight = 109.764F;
            this.classTimeDataGridViewTextBoxColumn3.HeaderText = "Лента";
            this.classTimeDataGridViewTextBoxColumn3.Name = "classTimeDataGridViewTextBoxColumn3";
            // 
            // classroomDataGridViewTextBoxColumn3
            // 
            this.classroomDataGridViewTextBoxColumn3.DataPropertyName = "Classroom";
            this.classroomDataGridViewTextBoxColumn3.FillWeight = 45.68528F;
            this.classroomDataGridViewTextBoxColumn3.HeaderText = "Аудитория";
            this.classroomDataGridViewTextBoxColumn3.Name = "classroomDataGridViewTextBoxColumn3";
            // 
            // teachSubjDataGridViewTextBoxColumn3
            // 
            this.teachSubjDataGridViewTextBoxColumn3.DataPropertyName = "TeachSubj";
            this.teachSubjDataGridViewTextBoxColumn3.FillWeight = 144.5508F;
            this.teachSubjDataGridViewTextBoxColumn3.HeaderText = "Преподаватель - Предмет";
            this.teachSubjDataGridViewTextBoxColumn3.Name = "teachSubjDataGridViewTextBoxColumn3";
            // 
            // groupDataGridViewTextBoxColumn3
            // 
            this.groupDataGridViewTextBoxColumn3.DataPropertyName = "Group";
            this.groupDataGridViewTextBoxColumn3.HeaderText = "Group";
            this.groupDataGridViewTextBoxColumn3.Name = "groupDataGridViewTextBoxColumn3";
            this.groupDataGridViewTextBoxColumn3.Visible = false;
            // 
            // scheduleDataGridViewTextBoxColumn3
            // 
            this.scheduleDataGridViewTextBoxColumn3.DataPropertyName = "Schedule";
            this.scheduleDataGridViewTextBoxColumn3.HeaderText = "Schedule";
            this.scheduleDataGridViewTextBoxColumn3.Name = "scheduleDataGridViewTextBoxColumn3";
            this.scheduleDataGridViewTextBoxColumn3.Visible = false;
            // 
            // idDataGridViewTextBoxColumn3
            // 
            this.idDataGridViewTextBoxColumn3.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn3.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn3.Name = "idDataGridViewTextBoxColumn3";
            this.idDataGridViewTextBoxColumn3.Visible = false;
            // 
            // classTimeDataGridViewTextBoxColumn2
            // 
            this.classTimeDataGridViewTextBoxColumn2.DataPropertyName = "ClassTime";
            this.classTimeDataGridViewTextBoxColumn2.FillWeight = 107.2761F;
            this.classTimeDataGridViewTextBoxColumn2.HeaderText = "Лента";
            this.classTimeDataGridViewTextBoxColumn2.Name = "classTimeDataGridViewTextBoxColumn2";
            // 
            // classroomDataGridViewTextBoxColumn2
            // 
            this.classroomDataGridViewTextBoxColumn2.DataPropertyName = "Classroom";
            this.classroomDataGridViewTextBoxColumn2.FillWeight = 45.68528F;
            this.classroomDataGridViewTextBoxColumn2.HeaderText = "Аудитория";
            this.classroomDataGridViewTextBoxColumn2.Name = "classroomDataGridViewTextBoxColumn2";
            // 
            // teachSubjDataGridViewTextBoxColumn2
            // 
            this.teachSubjDataGridViewTextBoxColumn2.DataPropertyName = "TeachSubj";
            this.teachSubjDataGridViewTextBoxColumn2.FillWeight = 147.0386F;
            this.teachSubjDataGridViewTextBoxColumn2.HeaderText = "Преподаватель - Предмет";
            this.teachSubjDataGridViewTextBoxColumn2.Name = "teachSubjDataGridViewTextBoxColumn2";
            // 
            // groupDataGridViewTextBoxColumn2
            // 
            this.groupDataGridViewTextBoxColumn2.DataPropertyName = "Group";
            this.groupDataGridViewTextBoxColumn2.HeaderText = "Group";
            this.groupDataGridViewTextBoxColumn2.Name = "groupDataGridViewTextBoxColumn2";
            this.groupDataGridViewTextBoxColumn2.Visible = false;
            // 
            // scheduleDataGridViewTextBoxColumn2
            // 
            this.scheduleDataGridViewTextBoxColumn2.DataPropertyName = "Schedule";
            this.scheduleDataGridViewTextBoxColumn2.HeaderText = "Schedule";
            this.scheduleDataGridViewTextBoxColumn2.Name = "scheduleDataGridViewTextBoxColumn2";
            this.scheduleDataGridViewTextBoxColumn2.Visible = false;
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.Visible = false;
            // 
            // classTimeDataGridViewTextBoxColumn4
            // 
            this.classTimeDataGridViewTextBoxColumn4.DataPropertyName = "ClassTime";
            this.classTimeDataGridViewTextBoxColumn4.FillWeight = 108.9264F;
            this.classTimeDataGridViewTextBoxColumn4.HeaderText = "Лента";
            this.classTimeDataGridViewTextBoxColumn4.Name = "classTimeDataGridViewTextBoxColumn4";
            // 
            // classroomDataGridViewTextBoxColumn4
            // 
            this.classroomDataGridViewTextBoxColumn4.DataPropertyName = "Classroom";
            this.classroomDataGridViewTextBoxColumn4.FillWeight = 45.68528F;
            this.classroomDataGridViewTextBoxColumn4.HeaderText = "Аудитория";
            this.classroomDataGridViewTextBoxColumn4.Name = "classroomDataGridViewTextBoxColumn4";
            // 
            // teachSubjDataGridViewTextBoxColumn4
            // 
            this.teachSubjDataGridViewTextBoxColumn4.DataPropertyName = "TeachSubj";
            this.teachSubjDataGridViewTextBoxColumn4.FillWeight = 145.3883F;
            this.teachSubjDataGridViewTextBoxColumn4.HeaderText = "Преподаватель - Предмет";
            this.teachSubjDataGridViewTextBoxColumn4.Name = "teachSubjDataGridViewTextBoxColumn4";
            // 
            // groupDataGridViewTextBoxColumn4
            // 
            this.groupDataGridViewTextBoxColumn4.DataPropertyName = "Group";
            this.groupDataGridViewTextBoxColumn4.HeaderText = "Group";
            this.groupDataGridViewTextBoxColumn4.Name = "groupDataGridViewTextBoxColumn4";
            this.groupDataGridViewTextBoxColumn4.Visible = false;
            // 
            // scheduleDataGridViewTextBoxColumn4
            // 
            this.scheduleDataGridViewTextBoxColumn4.DataPropertyName = "Schedule";
            this.scheduleDataGridViewTextBoxColumn4.HeaderText = "Schedule";
            this.scheduleDataGridViewTextBoxColumn4.Name = "scheduleDataGridViewTextBoxColumn4";
            this.scheduleDataGridViewTextBoxColumn4.Visible = false;
            // 
            // idDataGridViewTextBoxColumn4
            // 
            this.idDataGridViewTextBoxColumn4.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn4.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn4.Name = "idDataGridViewTextBoxColumn4";
            this.idDataGridViewTextBoxColumn4.Visible = false;
            // 
            // classTimeDataGridViewTextBoxColumn5
            // 
            this.classTimeDataGridViewTextBoxColumn5.DataPropertyName = "ClassTime";
            this.classTimeDataGridViewTextBoxColumn5.FillWeight = 113.0199F;
            this.classTimeDataGridViewTextBoxColumn5.HeaderText = "Лента";
            this.classTimeDataGridViewTextBoxColumn5.Name = "classTimeDataGridViewTextBoxColumn5";
            // 
            // classroomDataGridViewTextBoxColumn5
            // 
            this.classroomDataGridViewTextBoxColumn5.DataPropertyName = "Classroom";
            this.classroomDataGridViewTextBoxColumn5.FillWeight = 45.68528F;
            this.classroomDataGridViewTextBoxColumn5.HeaderText = "Аудитория";
            this.classroomDataGridViewTextBoxColumn5.Name = "classroomDataGridViewTextBoxColumn5";
            // 
            // teachSubjDataGridViewTextBoxColumn5
            // 
            this.teachSubjDataGridViewTextBoxColumn5.DataPropertyName = "TeachSubj";
            this.teachSubjDataGridViewTextBoxColumn5.FillWeight = 141.2948F;
            this.teachSubjDataGridViewTextBoxColumn5.HeaderText = "Преподаватель - Предмет";
            this.teachSubjDataGridViewTextBoxColumn5.Name = "teachSubjDataGridViewTextBoxColumn5";
            // 
            // groupDataGridViewTextBoxColumn5
            // 
            this.groupDataGridViewTextBoxColumn5.DataPropertyName = "Group";
            this.groupDataGridViewTextBoxColumn5.HeaderText = "Group";
            this.groupDataGridViewTextBoxColumn5.Name = "groupDataGridViewTextBoxColumn5";
            this.groupDataGridViewTextBoxColumn5.Visible = false;
            // 
            // scheduleDataGridViewTextBoxColumn5
            // 
            this.scheduleDataGridViewTextBoxColumn5.DataPropertyName = "Schedule";
            this.scheduleDataGridViewTextBoxColumn5.HeaderText = "Schedule";
            this.scheduleDataGridViewTextBoxColumn5.Name = "scheduleDataGridViewTextBoxColumn5";
            this.scheduleDataGridViewTextBoxColumn5.Visible = false;
            // 
            // idDataGridViewTextBoxColumn5
            // 
            this.idDataGridViewTextBoxColumn5.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn5.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn5.Name = "idDataGridViewTextBoxColumn5";
            this.idDataGridViewTextBoxColumn5.Visible = false;
            // 
            // FormSchRedact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 721);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormSchRedact";
            this.Text = "FormSchRedact";
            this.Load += new System.EventHandler(this.FormSchRedact_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureBindingSource5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal System.Windows.Forms.ComboBox comboBox1;
        protected internal System.Windows.Forms.Button button1;
        protected internal System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        protected internal System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource lectureBindingSource;
        private System.Windows.Forms.BindingSource lectureBindingSource5;
        private System.Windows.Forms.BindingSource lectureBindingSource4;
        private System.Windows.Forms.BindingSource lectureBindingSource3;
        private System.Windows.Forms.BindingSource lectureBindingSource2;
        private System.Windows.Forms.BindingSource lectureBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn classTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn classroomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teachSubjDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scheduleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn classTimeDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn classroomDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn teachSubjDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn scheduleDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn classTimeDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn classroomDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn teachSubjDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn scheduleDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn classTimeDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn classroomDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn teachSubjDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn scheduleDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn classTimeDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn classroomDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn teachSubjDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn scheduleDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn classTimeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn classroomDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn teachSubjDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn scheduleDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
    }
}