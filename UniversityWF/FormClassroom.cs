﻿using Demo.Domain;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormClassroom : Form
    {
        public FormClassroom()
        {
            InitializeComponent();
        }

        private void FormClassroom_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.ClassroomsRepository.AllItems.ToList();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            FormCRRedact formCRRedact = new FormCRRedact();
            DialogResult result = formCRRedact.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            Classroom ob = new Classroom();
            ob.ClassroomNumber = Convert.ToInt32(formCRRedact.textBox1.Text);
            Unit.ClassroomsRepository.AddItem(ob);
            Unit.ClassroomsRepository.SaveChanges();
            dataGridView1.DataSource = Unit.ClassroomsRepository.AllItems.ToList();
            MessageBox.Show("Новый объект добавлен");
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Unit.ClassroomsRepository.DeleteItem(id);
                Unit.ClassroomsRepository.SaveChanges();
                dataGridView1.DataSource = Unit.ClassroomsRepository.AllItems.ToList();
                MessageBox.Show("Объект удален");
            }
        }
    }
}
