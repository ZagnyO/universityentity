﻿using Demo.Domain;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormMark : Form
    {
        public FormMark()
        {
            InitializeComponent();
        }

        private void FormMarc_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.MarksRepository.AllItems.ToList();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            FormMarkRedact formMarkRedact = new FormMarkRedact();

            formMarkRedact.comboBox1.DataSource = Unit.StudentsRepository.AllItems.ToList();
            formMarkRedact.comboBox2.DataSource = Unit.TeachSubjRepository.AllItems.ToList();
            DialogResult result = formMarkRedact.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            Mark ob = new Mark();
            ob.StudentsMark = Convert.ToInt32(formMarkRedact.textBox1.Text); 
            ob.DataMark = formMarkRedact.dateTimePicker1.Value.Date;
            ob.Student = (Student)formMarkRedact.comboBox1.SelectedItem;
            ob.TeachSubj = (TeachSubj)formMarkRedact.comboBox2.SelectedItem;

            Unit.MarksRepository.AddItem(ob);
            Unit.MarksRepository.SaveChanges();
            MessageBox.Show("Новый объект добавлен");
            dataGridView1.DataSource = Unit.MarksRepository.AllItems.ToList();
           
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Mark ob = new Mark();
                ob = Unit.MarksRepository.GetItem(id);
                FormMarkRedact formMarkRedact = new FormMarkRedact();
                formMarkRedact.comboBox1.DataSource = Unit.StudentsRepository.AllItems.ToList();
                formMarkRedact.comboBox2.DataSource = Unit.TeachSubjRepository.AllItems.ToList();
                formMarkRedact.textBox1.Text = Convert.ToString(ob.StudentsMark);
                formMarkRedact.dateTimePicker1.Value = ob.DataMark;
                formMarkRedact.comboBox1.SelectedItem = ob.Student;
                formMarkRedact.comboBox2.SelectedItem = ob.TeachSubj;

                DialogResult result = formMarkRedact.ShowDialog(this);
                if (result == DialogResult.Cancel)
                    return;

                ob.StudentsMark = Convert.ToInt32(formMarkRedact.textBox1.Text);
                ob.DataMark = formMarkRedact.dateTimePicker1.Value.Date;
                ob.Student = (Student)formMarkRedact.comboBox1.SelectedItem;
                ob.TeachSubj = (TeachSubj)formMarkRedact.comboBox2.SelectedItem;
                Unit.MarksRepository.SaveChanges();
                dataGridView1.Refresh();
                MessageBox.Show("Объект обновлен");
            }
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Unit.MarksRepository.DeleteItem(id);
                Unit.MarksRepository.SaveChanges();
                MessageBox.Show("Объект удален");
                dataGridView1.DataSource = Unit.MarksRepository.AllItems.ToList();
            }
        }
    }
}
