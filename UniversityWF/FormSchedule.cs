﻿using Demo.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormSchedule : Form
    {
        public FormSchedule()
        {
            InitializeComponent();
        }

        private void FormSchedule_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.SchedulesRepository.AllItems.ToList();
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            FormLecture formLecture = new FormLecture();
            DialogResult result = formLecture.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;
        }
    }
}
