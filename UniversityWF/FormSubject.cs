﻿using Demo.Domain;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityWF
{
    public partial class FormSubject : Form
    {
        public FormSubject()
        {
            InitializeComponent();
        }

        private void FormSubject_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Unit.SubjectsRepository.AllItems.ToList();
            int index = dataGridView1.SelectedRows[0].Index;
            int id = 0;
            bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
            if (converted == false)
                return;
              listBox1.DataSource = Unit.TeachSubjRepository.AllItems.Where(x => x.Subject.Id == id).Select(t=>t.Teacher) .ToList();
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormSubRedact formSubRedact = new FormSubRedact();
            DialogResult result = formSubRedact.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            Subject subject = new Subject();
            subject.Name = formSubRedact.textBox1.Text;

            Unit.SubjectsRepository.AddItem(subject);
            Unit.SubjectsRepository.SaveChanges();
            dataGridView1.DataSource = Unit.SubjectsRepository.AllItems.ToList();
            MessageBox.Show("Новый объект добавлен");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString(), out id);
                if (converted == false)
                    return;
                MessageBox.Show("Вы дествительно хотите удалить выбранный объект");
                Unit.SubjectsRepository.DeleteItem(id);
                Unit.SubjectsRepository.SaveChanges();
                dataGridView1.DataSource = Unit.SubjectsRepository.AllItems.ToList();
                MessageBox.Show("Объект удален");
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            int index = 0;
            int id = 0;
            if (dataGridView1.SelectedRows.Count > 0)
            {
                index = dataGridView1.SelectedRows[0].Index;
                id = Int32.Parse(dataGridView1[dataGridView1.ColumnCount - 1, index].Value.ToString());
            }
            else
                return;
            listBox1.DataSource = Unit.TeachSubjRepository.AllItems.Where(x => x.Subject.Id == id).Select(t => t.Teacher).ToList();
        }
    }
}

