﻿using Demo.Domain.Common;
using Domain.lib.Entities;
using Domain.lib.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Repositories
{
    public class SubjectsRepository : DbRepository<Subject>, ISubjectsRepository
    {
        public SubjectsRepository(DbContext context): base(context)
        {

        }
    }
}
