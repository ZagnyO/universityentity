﻿using Demo.Domain.Abstract;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Repositories
{
    public interface ITeachSubjRepository : IDbRepository<TeachSubj>
    {
    }
}
