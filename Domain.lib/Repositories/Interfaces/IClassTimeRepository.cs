﻿using Demo.Domain.Abstract;
using Domain.lib.Entities;

namespace Domain.lib.Repositories
{
    public interface IClassTimeRepository: IDbRepository<ClassTime>
    {
    }
}
