﻿using Demo.Domain.Abstract;
using Demo.Domain.Common;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Repositories
{
    public interface ISpecialitiesRepository :  IDbRepository<Speciality>
    {
    }
}
