﻿using Demo.Domain.Abstract;
using Demo.Domain.Entities;


namespace Demo.Domain.Repositories
{
    public interface ITeachersRepository:IDbRepository<Teacher>
    {
    }
}
