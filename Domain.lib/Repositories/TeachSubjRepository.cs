﻿using Demo.Domain.Common;
using Domain.lib.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Repositories
{
    public class TeachSubjRepository : DbRepository<TeachSubj>, ITeachSubjRepository
    {
        public TeachSubjRepository(DbContext context) : base(context)
        {
        }
    }
}
