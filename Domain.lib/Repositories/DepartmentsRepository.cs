﻿using Demo.Domain.Common;
using Demo.Domain.Entities;
using System.Data.Entity;

namespace Demo.Domain.Repositories
{
    public class DepartmentsRepository : DbRepository<Department>, IDepartmentsRepository
    {
        public DepartmentsRepository(DbContext context) : base(context)
        {
        }
    }
}
