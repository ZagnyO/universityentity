﻿using Demo.Domain.Common;
using Demo.Domain.Entities;
using Demo.Domain.Repositories;
using Domain.lib.Entities;
using System.Data.Entity;


namespace Domain.lib.Repositories
{
    public class SchedulesRepository : DbRepository<Schedule>, ISchedulesRepository
    {
        public SchedulesRepository(DbContext context) : base(context)
        {
        }
    }
}
