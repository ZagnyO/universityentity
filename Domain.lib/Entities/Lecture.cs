﻿using Demo.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Entities
{
    [Table("tbLectures")]
    public class Lecture : DbEntity
    {
        public virtual ClassTime ClassTime { get; set; }
        public virtual Classroom Classroom { get; set; }
        public virtual TeachSubj TeachSubj { get; set; }
        public virtual Group Group { get; set; }
        public virtual Schedule Schedule { get; set; }
        public override string ToString()
        {
            return $"{ ClassTime} {Classroom} {TeachSubj} ";
        }
    }
}
