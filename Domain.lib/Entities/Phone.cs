﻿using Demo.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Entities
{
    [Table("tbPhones")]
    public class Phone : DbEntity
    {
        [StringLength(64)]
        public string StudentsPhone { get; set; }
        public virtual Student Student { get; set; }
        public override string ToString()
        {
            return StudentsPhone;
        }
    }
}
