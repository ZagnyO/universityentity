﻿using Demo.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Entities
{
    [Table("tbMarks")]
    public class Mark : DbEntity
    {
        [Range(0, 12)]
        public int StudentsMark { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DataMark { get; set; }
        public virtual Student Student { get; set; }
        public virtual TeachSubj TeachSubj { get; set; }
        public override string ToString()
        {
            return $"{TeachSubj}    {StudentsMark}    {DataMark} ";
           
        }
    }    
}
