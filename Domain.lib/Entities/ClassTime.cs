﻿using Demo.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.lib.Entities
{
    public class ClassTime : DbEntity
    {
        public int Number { get; set; }
    //    [Column(TypeName = "datetime")]
        public string StartLecture { get; set; }
        // [Column(TypeName = "datetime")] DateTime 
        public  string FinishLecture { get; set; }
        public override string ToString()
        {
            return $"{Number}    {StartLecture}   {FinishLecture}";
        }
    }
}
