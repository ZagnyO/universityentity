﻿using Demo.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Entities
{
    [Table("tbSchedule")]
    public class Schedule : DbEntity
    {
        public string DayWeek { get; set; }
        public virtual List<Lecture> Lectures { get; set; }
        //public Schedule()
        //{
        //    List<string> DayWeek = new List<string> {"Понедельник",
        //                                             "Вторник",
        //                                             "Среда",
        //                                             "Четверг",
        //                                             "Пятница",
        //                                             "Суббота",
        //                                             "Воскресенье"};
        //}
        public override string ToString()
        {
            return $"  {DayWeek}  ";
        }
    }
}
