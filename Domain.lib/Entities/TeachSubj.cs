﻿using Demo.Domain.Abstract;
using Demo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.lib.Entities
{
    [Table("tbTeachSubj")]
    public class TeachSubj : DbEntity
    {
        public virtual Teacher Teacher { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual List<Mark> Marks { get; set; }
        public override string ToString()
        {
            return $"  {Teacher}  \t  {Subject}";
        }
    }
}
