﻿using Demo.Domain.App.Context;
using Demo.Domain.Repositories;
using Domain.lib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Domain
{
    public static class Unit
    {
        private static MyAppDbContext _context;
        static Unit()
        {
            _context = new MyAppDbContext("MyAppConnStr");
            ClassTimeRepository = new ClassTimeRepository(_context);
            ClassroomsRepository = new ClassroomsRepository(_context);
            DepartmentsRepository = new DepartmentsRepository(_context);
            GroupsRepository = new GroupsRepository(_context);
            LecturesRepository = new LecturesRepository(_context);
            MarksRepository = new MarksRepository(_context);
            PhonesRepository = new PhonesRepository(_context);
            SchedulesRepository = new SchedulesRepository(_context);
            SpecialitiesRepository = new SpecialitiesRepository(_context);
            StudentsRepository = new StudentsRepository(_context);
            SubjectsRepository = new SubjectsRepository(_context);
            TeachersRepository = new TeachersRepository(_context);
            TeachSubjRepository = new TeachSubjRepository(_context);

        }
        public static ClassTimeRepository ClassTimeRepository { get; }
        public static ClassroomsRepository ClassroomsRepository { get; }
        public static DepartmentsRepository DepartmentsRepository { get; }
        public static GroupsRepository GroupsRepository { get; }
        public static LecturesRepository LecturesRepository { get; }
        public static MarksRepository MarksRepository { get; }
        public static PhonesRepository PhonesRepository { get; }
        public static SpecialitiesRepository SpecialitiesRepository { get; }
        public static SchedulesRepository SchedulesRepository { get; }
        public static StudentsRepository StudentsRepository { get; }
        public static SubjectsRepository SubjectsRepository { get; }
        public static TeachSubjRepository TeachSubjRepository { get; }
        public static TeachersRepository TeachersRepository { get; }
    }
}
