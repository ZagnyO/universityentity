﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Demo.Domain.Entities;
using Domain.lib.Entities;

namespace Demo.Domain.App.Context
{
    public class MyAppDbContext:DbContext
    {
        public DbSet<ClassTime> ClassTime { get; set; }
        public DbSet<Classroom> Classrooms { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Lecture> Lectures { get; set; }
        public DbSet<Mark> Marks { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<TeachSubj> TeachSubjs { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Schedule>Schedules { get; set; }

        public MyAppDbContext(string ConnStrOrDatabaseName)
                       : base(ConnStrOrDatabaseName)
        {            
            Database.SetInitializer(new MyAppDbContextInitializer());
        }
    }
    //DropCreateDatabaseAlways<MyAppDbContext>          пересоздает всегда
    //DropCreateDatabaseIfModelChanges<MyAppDbContext>  пересоздает, если внесены изменения
    //DropCreateDatabaseIfNotExist<MyAppDbContext>      создает, если базы нет

    public class MyAppDbContextInitializer: DropCreateDatabaseIfModelChanges<MyAppDbContext>
    {
        protected override void Seed(MyAppDbContext context)
        {
            context.Departments.Add(new Department { Name = "Programming" });
            context.Departments.Add(new Department { Name = "Testing" });
            context.Departments.Add(new Department { Name = "Design" });
            context.SaveChanges();

            context.Specialities.Add(new Speciality { Name = "Testing" });
            context.Specialities.Add(new Speciality { Name = "Design" });
            context.Specialities.Add(new Speciality { Name = "Front End" });
            context.SaveChanges();

            context.Groups.Add(new Group { Name = "TS-18-1",
                Speciality = Unit.SpecialitiesRepository.AllItems.FirstOrDefault(x => x.Name == "Testing")});
            context.Groups.Add(new Group { Name = "TS-18-2",
                Speciality = Unit.SpecialitiesRepository.AllItems.FirstOrDefault(x => x.Name == "Testing")});
            context.Groups.Add(new Group { Name = "D-18-1",
                Speciality = Unit.SpecialitiesRepository.AllItems.FirstOrDefault(x => x.Name == "Design")});
            context.Groups.Add(new Group { Name = "FE-18-1",
                Speciality = Unit.SpecialitiesRepository.AllItems.FirstOrDefault(x => x.Name == "Front End")});
            context.SaveChanges();

            context.Classrooms.Add(new Classroom { ClassroomNumber = 1 });
            context.Classrooms.Add(new Classroom { ClassroomNumber = 2 });
            context.Classrooms.Add(new Classroom { ClassroomNumber = 3 });
            context.Classrooms.Add(new Classroom { ClassroomNumber = 4 });
            context.SaveChanges();
            //context.ClassTime.Add(new ClassTime
            //{ Number = 1,
            //    StartLecture = Convert.ToDateTime("08:30:00"),
            //    FinishLecture = Convert.ToDateTime("09:50:00")
            //});
            //context.ClassTime.Add(new ClassTime
            //{   Number = 2,
            //    StartLecture = Convert.ToDateTime("10:00:00"),
            //    FinishLecture = Convert.ToDateTime("11:20:00")
            //});
            //context.ClassTime.Add(new ClassTime
            //{
            //    Number = 3,
            //    StartLecture = Convert.ToDateTime("11:45:00"),
            //    FinishLecture = Convert.ToDateTime("13:05:00")
            //});
            //context.ClassTime.Add(new ClassTime
            //{
            //    Number = 4,
            //    StartLecture = Convert.ToDateTime("13:20:00"),
            //    FinishLecture = Convert.ToDateTime("14:40:00")
            //});
            //context.ClassTime.Add(new ClassTime
            //{
            //    Number = 5,
            //    StartLecture = Convert.ToDateTime("14:50:00"),
            //    FinishLecture = Convert.ToDateTime("16:10:00")
            //});
            context.ClassTime.Add(new ClassTime
            {
                Number = 1,
                StartLecture = "08:30",
                FinishLecture = "09:50"
            });
            context.ClassTime.Add(new ClassTime
            {
                Number = 2,
                StartLecture = "10:00",
                FinishLecture = "11:20"
            });
            context.ClassTime.Add(new ClassTime
            {
                Number = 3,
                StartLecture = "11:45",
                FinishLecture = "13:05"
            });
            context.ClassTime.Add(new ClassTime
            {
                Number = 4,
                StartLecture = "13:20",
                FinishLecture = "14:40"
            });
            context.ClassTime.Add(new ClassTime
            {
                Number = 5,
                StartLecture = "14:50",
                FinishLecture = "16:10"
            });
            context.SaveChanges();

            context.Subjects.Add(new Subject { Name = "C#" });
            context.Subjects.Add(new Subject { Name = "Angular" });
            context.Subjects.Add(new Subject { Name = "QA" });
            context.Subjects.Add(new Subject { Name = "Design" });
            context.SaveChanges();
            //Teacher t1 = new Teacher
            //{
            //    FirstName = "Олег",
            //    MiddleName = "Гаврилович",
            //    LastName = "Гоман",
            //    Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "Testing")
            //};
            //Teacher t2 = new Teacher
            //{
            //    FirstName = "Александр",
            //    MiddleName = "Борисович",
            //    LastName = "Кобельчак",
            //    Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "Programming")
            //};
            //Teacher t3 = new Teacher
            //{
            //    FirstName = "Николай",
            //    MiddleName = "Николаевич",
            //    LastName = "Лычагин",
            //    Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "Programming")
            //};
            //Teacher t4 = new Teacher
            //{
            //    FirstName = "Мария",
            //    MiddleName = "Борисовна",
            //    LastName = "Рахманова",
            //    Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "Design")
            //};


            context.Teachers.Add(new Teacher
            {
                FirstName = "Олег",
                MiddleName = "Гаврилович",
                LastName = "Гоман",
                Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "Testing")
            });
            context.Teachers.Add(new Teacher
            {
                FirstName = "Александр",
                MiddleName = "Борисович",
                LastName = "Кобельчак",
                Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "Programming")
            });
            context.Teachers.Add(new Teacher
            {
                FirstName = "Николай",
                MiddleName = "Николаевич",
                LastName = "Лычагин",
                Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "Programming")
            });
            context.Teachers.Add(new Teacher
            {
                FirstName = "Мария",
                MiddleName = "Борисовна",
                LastName = "Рахманова",
                Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "Design")
            });
            context.SaveChanges();

            //TeachSubj ts1 = new TeachSubj
            //{
            //    Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Кобельчак"),
            //    Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "C#")
            //};
            //TeachSubj ts2 = new TeachSubj
            //{
            //    Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Кобельчак"),
            //    Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "Angular")
            //};
            //TeachSubj ts3 = new TeachSubj
            //{
            //    Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Гоман"),
            //    Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "QA")
            //};
            //TeachSubj ts4 = new TeachSubj
            //{
            //    Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Лычагин"),
            //    Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "C#")
            //};
            //TeachSubj ts5 = new TeachSubj
            //{
            //    Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Рахманова"),
            //    Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "Design")
            //};
            //t1.Add(ts3);
            context.TeachSubjs.Add(new TeachSubj
            {
                Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Кобельчак"),
                Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "C#")
            });
            context.TeachSubjs.Add(new TeachSubj
            {
                Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Кобельчак"),
                Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "Angular")
            });
            context.TeachSubjs.Add(new TeachSubj
            {
                Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Гоман"),
                Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "QA")
            });
            context.TeachSubjs.Add(new TeachSubj
            {
                Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Лычагин"),
                Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "C#")
            });
            context.TeachSubjs.Add(new TeachSubj
            {
                Teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.LastName == "Рахманова"),
                Subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Name == "Design")
            });
                  
            context.SaveChanges();
            context.Teachers.Include(t => t.TeachSubjs);
            context.Subjects.Include(t => t.TeachSubjs);
            context.Students.Add(new Student
            {
                FirstName = "Сидор",
                MiddleName = "Сидорович",
                LastName = "Сидоров",
                Birthday = Convert.ToDateTime("07.01.2000"),
                LogbookNumber = 2304,
                Email = "SidorovSS@gmai.com",
                Address = "Яворницкого 52/40, Днепр",
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "TS-18-1")
            });

            context.Students.Add(new Student
            {
                FirstName = "Иван",
                MiddleName = "Иванович",
                LastName = "Иванов",
                Birthday = Convert.ToDateTime("26.09.2000"),
                LogbookNumber = 2305,
                Email = "IvanovII@gmai.com",
                Address = "Яворницкого 52/41, Днепр",
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "D-18-1")
            });
            context.Students.Add(new Student
            {
                FirstName = "Петр",
                MiddleName = "Петрович",
                LastName = "Петров",
                Birthday = Convert.ToDateTime("26.07.2001"),
                LogbookNumber = 2301,
                Email = "PetrovPP@gmai.com",
                Address = "Яворницкого 52/41, Днепр",
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "FE-18-1")
            });
            context.Students.Add(new Student
            {
                FirstName = "Сергей",
                MiddleName = "Сергеевич",
                LastName = "Сергеев",
                Birthday = Convert.ToDateTime("2.11.2001"),
                LogbookNumber = 2305,
                Email = "SergeevSS@gmai.com",
                Address = "Яворницкого 52/41, Днепр",
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "FE-18-1")
            });
            context.SaveChanges();

            context.Schedules.Add(new Schedule { DayWeek = "Понедельник" });
            context.Schedules.Add(new Schedule { DayWeek = "Вторник" });
            context.Schedules.Add(new Schedule { DayWeek = "Среда" });
            context.Schedules.Add(new Schedule { DayWeek = "Четверг" });
            context.Schedules.Add(new Schedule { DayWeek = "Пятница" });
            context.Schedules.Add(new Schedule { DayWeek = "Суббота" });
            context.Schedules.Add(new Schedule { DayWeek = "Воскресенье" });
            context.SaveChanges();
           
            context.Phones.Add(new Phone
            {
                StudentsPhone= "091-304-11-08",
                Student = Unit.StudentsRepository.AllItems.FirstOrDefault(x => x.FirstName == "Сидор"&& x.MiddleName == "Сидорович"&& x.LastName == "Сидоров")
            });
            context.Phones.Add(new Phone
            {
                StudentsPhone = "063-472-36-17",
                Student = Unit.StudentsRepository.AllItems.FirstOrDefault(x => x.FirstName == "Иван" && x.MiddleName == "Иванович" && x.LastName == "Иванов")
            });
            context.Phones.Add(new Phone
            {
                StudentsPhone = "096-566-13-08",
                Student = Unit.StudentsRepository.AllItems.FirstOrDefault(x => x.FirstName == "Сидор" && x.MiddleName == "Сидорович" && x.LastName == "Сидоров")
            });
            context.Phones.Add(new Phone
            {
                StudentsPhone = "067-245-75-85",
                Student = Unit.StudentsRepository.AllItems.FirstOrDefault(x => x.FirstName == "Сергей" && x.MiddleName == "Сергеевич" && x.LastName == "Сергеев")
            });
            context.SaveChanges();


            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 1),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 1),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Кобельчак") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "C#")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "FE-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Понедельник")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 2),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 1),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Кобельчак") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "Angular")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "FE-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Понедельник")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 1),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 1),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Кобельчак") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "C#")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "FE-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Четверг")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 2),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 1),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Кобельчак") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "Angular")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "FE-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Четверг")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 1),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 2),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Гоман") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "QA")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "TS-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Вторник")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 1),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 2),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Лычагин") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "C#")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "TS-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Вторник")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 1),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 2),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Гоман") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "QA")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "TS-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Пятница")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 1),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 2),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Лычагин") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "C#")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "TS-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Пятница")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 1),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 3),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Рахманова") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "Design")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "D-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Среда")
            });
            context.Lectures.Add(new Lecture
            {
                ClassTime = Unit.ClassTimeRepository.AllItems.FirstOrDefault(x => x.Number == 1),
                Classroom = Unit.ClassroomsRepository.AllItems.FirstOrDefault(x => x.ClassroomNumber == 3),
                TeachSubj = Unit.TeachSubjRepository.AllItems.FirstOrDefault(x => x.Teacher == Unit.TeachersRepository.AllItems.FirstOrDefault(n => n.LastName == "Рахманова") &&
                                                                                 x.Subject == Unit.SubjectsRepository.AllItems.FirstOrDefault(s => s.Name == "Design")),
                Group = Unit.GroupsRepository.AllItems.FirstOrDefault(x => x.Name == "D-18-1"),
                Schedule = Unit.SchedulesRepository.AllItems.FirstOrDefault(x => x.DayWeek == "Пятница")
            });

        }
    }
}
