﻿
using System.ComponentModel.DataAnnotations;

namespace Demo.Domain.Abstract
{
    public interface IDbEntity
    {
        [Key]
        int Id { get; set; }
    }
}
