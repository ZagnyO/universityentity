﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Demo.Domain.App.Context;
using Demo.Domain.Entities;

namespace Demo.Domain
{
    class Program
    {
        static void Main(string[] args)
        {
 //           MyAppDbContext context = new MyAppDbContext("TestDatabaseEntity");

            Unit.TeachersRepository.AddItem(new Teacher
            {
                FirstName = "Олег",
                MiddleName = "Гаврилович",
                LastName = "Гоман",
                Department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Name == "PROGRAMMING")
            });
            // Console.WriteLine("{0}.{1} - {2}", ute.Id, u.Name, u.Age);
            Console.WriteLine(Unit.TeachersRepository.AllItems.ToString());
            Console.ReadKey();
        }
    }
}
